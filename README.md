# EE Gallery Grid #

A grid gallery based on Photoswipe.js with some a11y improvements - designed to be rolled out in a CMS.

### What is this repository for? ###

* [EE Gallery Grid](https://newsinteractives.cbc.ca/qa/ee-gallery/) is a stand-alone proof-of-concept for code that would be integrated into Expression Engine.
* The repo includes examples of four layouts: one for four images and one for five, and both can be flipped.
* Based on [PhotoSwipe Version 4.1.3](https://photoswipe.com/).
* Extended for accessibility using the suggestions of [Kazuhiko Tsuchiya](https://github.com/dimsemenov/PhotoSwipe/issues/1077).

### How do I get set up? ###

* There are some considerations with installing this gallery that make it better for a11y. 
* All the content on the page (in the main area) should be wrapped by a base div (div id="base") so it can be aria-hidden when the gallery is up.
* Galleries are comprised of a series of figures, inside parent divs with (div class="grid")
* The parent divs of each grid contain either four of five figures, depending on whether they are (div class="fourGallery") or (div class="fiveGallery")
* The parent divs also contain ids (e.g. "gallery1, gallery2").
* The first figure of each gallery includes a class that relates to the gallery ID (e.g. "focus_when_gallery1_closed").
* The header above the gallery has a unique ID that informs the aria value of the region where the gallery exists.
* Photoswipe.js has been modified to get the id of the gallery and add focus to the appropriate figure when the gallery is destroyed.
* This fixes a loss of focus on closing the gallery that would not be ideal for a11y.
* Photoswipe.js has also been modified to trap the tab within the gallery - so it doesn't leak out onto the page. 
* The root element of Photoswipe is always included on the page below the code that populates the galleries (div class="pswp"). 
* Some display options like bgOpacity are set in script.js, where zooming of images has been turned off.

### Who do I talk to? ###

* Code: Dwight Friesen and/or Matt Crider
* Design: Kelly Keenan
* Implementation: Mike Evans